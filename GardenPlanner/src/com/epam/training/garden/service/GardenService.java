/*EVALUATE PLANTS*/

package com.epam.training.garden.service;

import com.epam.training.garden.App;
import com.epam.training.garden.domain.GardenProperties;
import com.epam.training.garden.domain.PlantType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GardenService {
    private List<PlantType> plantTypes;
    private static GardenProperties properties;

    public GardenService() {
    }

    public static List<PlantType> getPlantTypes() {
        return new ArrayList<>(List.of(App.CORN,
                App.PUMPKIN,
                App.GRAPE,
                App.TOMATO,
                App.CUCUMBER));
    }

    public void setProperties(GardenProperties properties) {
        this.properties = properties;
    }

    public static Result evaluatePlan(Map<String, Integer> items){

        Result result = null;
        double totalArea = 0.0;
        double totalWater = 0.0;

        int count = 0;

        for (Map.Entry<String, Integer> plants : items.entrySet()) {
            String plantInput = plants.getKey();
            PlantType plantType = getPlant(plantInput);
            double area = plantType.getArea() * plants.getValue();
            double water = area * plantType.getWaterAmount();

            totalArea = totalArea + area;
            totalWater = totalWater + water;
        }
        
        return new Result(totalArea, totalWater, totalArea < properties.getArea(), totalWater < properties.getWaterSupply());
    }

    private static PlantType getPlant(String plantInput) {
        List<PlantType> plantsKnown = getPlantTypes();
        PlantType plantUser = null;

        for (PlantType plant: plantsKnown) {
            if(plantInput.equalsIgnoreCase(plant.getName())){
                plantUser = plant;
            }
        }

        if(plantUser == null){
            throw new IllegalArgumentException("Unknown plant: " + plantInput + ".");
        }

        return plantUser;
    }

    @Override
    public String toString() {
        return "GardenService{" +
                "plantTypes=" + plantTypes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GardenService that = (GardenService) o;
        return plantTypes.equals(that.plantTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plantTypes);
    }
}
