package com.epam.training.garden;

import com.epam.training.garden.domain.GardenProperties;
import com.epam.training.garden.domain.PlantType;
import com.epam.training.garden.service.GardenService;
import com.epam.training.garden.service.Result;

import java.util.*;

public class App {

    public static final PlantType CORN = new PlantType("Corn", 0.4, 10);
    public static final PlantType PUMPKIN = new PlantType("Pumpkin", 2, 5);
    public static final PlantType GRAPE = new PlantType("Grape", 3, 5);
    public static final PlantType TOMATO = new PlantType("Tomato", 0.3, 10);
    public static final PlantType CUCUMBER = new PlantType("Cucumber", 0.4, 10);

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("***Welcome to Garden Planner***");
        System.out.println("Please enter your garden properties.");
        try {
            System.out.print("Size (square meter): ");
            double squareMeter = sc.nextDouble();
            System.out.print("Water supply: ");
            double waterSupply = sc.nextDouble();
            sc.nextLine();

            GardenProperties gardProp = new GardenProperties(squareMeter, waterSupply);
            GardenService gardServ = new GardenService();
            gardServ.setProperties(gardProp);

            System.out.println("Know plant types: ");
            System.out.println("- " + CORN.getName());
            System.out.println("- " + PUMPKIN.getName());
            System.out.println("- " + GRAPE.getName());
            System.out.println("- " + TOMATO.getName());
            System.out.println("- " + CUCUMBER.getName());

            System.out.println("Please enter the plants you would like to put in your garden. Press enter when you are done.");

            Map<String, Integer> items = new HashMap<>();
            boolean input = true;
            String plantInput;
            while(input){
                System.out.print("Enter plant (format: name,amount): ");
                plantInput = sc.nextLine();
                if(plantInput.equals("")){
                    input = false;
                }else{
                    String[] plant = plantInput.split(",");
                    String name = plant[0];
                    int amount = Integer.parseInt(plant[1]);
                    items.put(name, amount);
                }
            }

            System.out.println("***Result***");
            Result result = GardenService.evaluatePlan(items);

            System.out.println("Required area: " + result.getArea() + "m2");
            System.out.println("Water need: " + result.getWaterAmount() + "l");

            if(result.isAreaOk() && result.isWaterOk()){
                System.out.println("Plant is feasible in your garden! :)");
            }else{
                System.out.println("Plant is not feasible in your garden! :(");
                System.out.println("- Not enough area.");
                System.out.println("- Not enough water.");
            }

        } catch (InputMismatchException e) {
            System.out.println("\nIllegal input format.");
        } catch (NumberFormatException e) {
            System.out.println("\nIllegal input format.");
        }
    }
}
